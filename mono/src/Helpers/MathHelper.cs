﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

public static class MathHelper
{
	public static double Clamp01(double value){
		return MathHelper.Clamp (value, 0.0, 1.0);
	}

	public static double Clamp(double value, double min, double max){
		if (value <= min) {
			return min;
		} else if (value >= max) {
			return max;
		} else {
			return value;
		}
	}

	public static double GetDistance(double radius, double arcAngle){
		return 2.0 * Math.PI * Math.Abs(radius) * Math.Abs(arcAngle) / 360.0;
	}

	public static double GetMaxSpeed(double radius, double grip)
	{
		// If the arc has an infinity radius, cap the radius to a very high value
		if (double.IsInfinity(radius) || double.IsNaN(radius) || radius > 10000.0){
			radius = 10000.0;
		}

		// Put a lower limit to the radius because the vehicle needs a minimum turning radius
		if (radius < 0.01){
			radius = 0.01;
		}

		// If the grip has an infinite value, cap the grip to a high value
		if (double.IsInfinity(grip) || double.IsNaN(grip) || grip > 10000.0){
			grip = 10000.0;
		}

		// Put a lower limit to the grip because the vehicle can't move without grip
		if (grip < 0.01){
			grip = 0.01;
		}

		// speed^2 / radius ==  * grip
		return Math.Sqrt(radius * grip);
	}

	///--------------------------------------------------------------------
	/// <summary>
	/// This method solves the equation:  a*x + b = 0
	/// </summary>
	/// <param name='a'>A.</param>
	/// <param name='b'>B.</param>
	/// <returns>The solution of the equation.</returns>
	///--------------------------------------------------------------------
	public static double SolveFirstDegreeEquation(double a, double b){
		if (double.IsPositiveInfinity(a) || double.IsNegativeInfinity(a)){
			// If the equation hasn't a valid solution...
			return double.NaN;

		}else if (Math.Abs(a) < double.Epsilon){
			if (Math.Abs(b) < double.Epsilon){
				// If the equation has infinite valid results, return 0
				// (which is also a valid solution)
				return 0.0;
			}else{
				// If the equation hasn't a valid solution...
				return double.NaN;
			}
		}

		// If the equation has a single result, return that result
		return -b / a;
	}

	///--------------------------------------------------------------------
	/// <summary>
	/// This method solves the equation:  a*x² + b*x + c = 0
	///
	/// x = (-b ± SQRT(b² - 4*a*c)) / (2*a)
	/// </summary>
	/// <param name='a'>A.</param>
	/// <param name='b'>B.</param>
	/// <param name='c'>B.</param>
	/// <returns>The solutions of the equation.</returns>
	///--------------------------------------------------------------------
	public static ReadOnlyCollection<double> SolveQuadraticEquation(
		double a, 
		double b, 
		double c
	){
		// Check if it is really a Quadratic Equation
		if (Math.Abs(a) < double.Epsilon){
			// If it's a First Degree Equation, solve that equation
			double solution = MathHelper.SolveFirstDegreeEquation(b, c);

			// If the First Degree Equation has a solution...
			if (!double.IsNaN(solution)){
				// Return that solution
				return new ReadOnlyCollection<double>(new double[]{solution});
			}
		}else{
			// If it is a Quadratic Equation...
			double discriminant = b * b - 4.0 * a * c;

			// Check if the equation has a single solution, if it has two
			// different solutions or if it hasn't any valid solution...
			if (Math.Abs(discriminant) < double.Epsilon){
				// Single solution
				return new ReadOnlyCollection<double>(new double[]{-b / (2.0 * a)});

			}else if (discriminant > 0.0){
				// Two solutions
				double sqrt = Math.Sqrt(discriminant);
				double denominator = 2.0 * a;

				return new ReadOnlyCollection<double>(new double[]{
					(-b + sqrt) / denominator,
					(-b - sqrt) / denominator
				});
			}
			Console.WriteLine ("a = " + a + "\tb = " + b + "\tc = " + c + "\t|||||\t" + discriminant);
		}


		// No solution
		return new ReadOnlyCollection<double>(new double[0]);
	}
}

