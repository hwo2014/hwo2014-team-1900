using System;
using System.IO;
using System.Net.Sockets;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

public class Bot {
	#region protected instance fields
	// This field is required by the Machine Learning Algorithm
	protected AI _ai;
	#endregion

	#region constructor
	public Bot(AI ai, StreamReader reader, StreamWriter writer, JoinMsg join) {
		string line = null;
		bool exit = false;
		this._ai = ai;

		// Sned the "Join" message
		Bot.Send(writer, join);
		bool changed = false;

		while (!exit){
			while ((line = reader.ReadLine ()) != null) {
				JObject json = JObject.Parse (line);
				string gameId = null;
				int gameTick = 0;
				JToken token = null;

				MsgWrapper msg = new MsgWrapper (
					json ["msgType"].ToString (),
					json ["data"]
				);

				if (json != null && json.TryGetValue ("gameId", out token) && token != null) {
					gameId = token.ToString();
				}

				if (json != null && json.TryGetValue ("gameTick", out token) && token != null) {
					gameTick = int.Parse (token.ToString ());
				}

				//Console.WriteLine (msg.MsgType + " --- " + gameId + " --- " + gameTick);
				if (string.Equals (msg.MsgType, MsgType.CarPositions)) {
					ai.OnUpdate(new CarPositionsMsg (msg.Data as JArray, gameId, gameTick));

					Console.WriteLine (
						"SPEED: " + ai.Vehicle.CurrentSpeed + 
						"\t| TARGET: " + ai.TargetSpeed +
						"\t| ANGLE: " + ai.Vehicle.CurrentPosition.Angle 
						+
						"\t| PREDICTED ANGLE: " + ai.VehiclePhysics.CalculateDrifting(
							ai.Vehicle.CurrentSpeed
							,
							ai.Track.GetSector(
								ai.Vehicle.CurrentPosition.StartLane,
								ai.Vehicle.CurrentPosition.CurrentPieceIndex
							).Radius
							,
							ai.Vehicle.PreviousPosition.Angle
						)
						+
						"\t| TRACK ANGLE: " + ai.Track.GetSector(
							ai.Vehicle.CurrentPosition.StartLane,
							ai.Vehicle.CurrentPosition.CurrentPieceIndex
						).Angle
						+
						"\t| THROTTLE: " + ai.Throttle 
					);

					if (!changed && ai.Vehicle.CurrentSpeed > 0.0) {
						//Bot.Send (writer, new SwitchLaneMsg (SwitchLaneDirection.Right));
						Bot.Send (writer, new ThrottleMsg (ai.Throttle));
						changed = true;
					} else {
						Bot.Send (writer, new ThrottleMsg (ai.Throttle));
					}
				
				}else if (string.Equals (msg.MsgType, MsgType.Crash)) {
					ai.OnCrash(new CrashMsg (msg.Data as JObject, gameId, gameTick));

					if (ai.LearningMode) {
						return;
					}

				} else if (string.Equals(msg.MsgType, MsgType.Disqualified)){
					ai.OnDisqualified(new DisqualifiedMsg (msg.Data as JObject, gameId, gameTick));

				} else if (string.Equals (msg.MsgType, MsgType.GameFinished)) {
					ai.OnGameFinished( new GameFinishedMsg (msg.Data as JObject) );
					Bot.Send(writer, new PingMsg ());

				} else if (string.Equals (msg.MsgType, MsgType.GameInitialized)) {
					ai.OnGameInitialized( new GameInitializedMsg (msg.Data as JObject) );
					Bot.Send(writer, new PingMsg ());

				} else if (string.Equals (msg.MsgType, MsgType.GameStarted)) {
					ai.OnGameStarted(new GameStartedMsg ());
					Bot.Send(writer, new PingMsg ());

				}else if (string.Equals (msg.MsgType, MsgType.Joined)) {
					ai.OnJoined(new JoinedMsg (msg.Data as JObject));
					Bot.Send(writer, new PingMsg ());

				} else if (string.Equals (msg.MsgType, MsgType.LapFinished)) {
					ai.OnLapFinished(new LapFinishedMsg (msg.Data as JObject, gameId, gameTick));

				} else if (string.Equals (msg.MsgType, MsgType.RaceFinished)) {
					ai.OnRaceFinished(new RaceFinishedMsg (msg.Data as JObject, gameId, gameTick));

				}else if (string.Equals (msg.MsgType, MsgType.Spawn)) {
					ai.OnSpawn(new SpawnMsg (msg.Data as JObject, gameId, gameTick));

				} else if (string.Equals(msg.MsgType, MsgType.TournamentFinished)){
					new TournamentFinishedMsg ();
					exit = true;
				} else if (string.Equals(msg.MsgType, MsgType.Error)){
					Console.WriteLine("Error: " + msg.Data);
					exit = true;
				} else {
					Bot.Send(writer, new PingMsg ());
				}
			}
			//Thread.Sleep (10);
		}

		Console.WriteLine (ai.Vehicle.TotalDistance + " ----> ");
		foreach (RacingLine r in ai.Track.RacingLines) {
			Console.WriteLine (3.0 * r.TotalDistance);
		}

		foreach (CrashInformation crash in ai.Crashes) {
			Console.WriteLine (
				"CRASH: " +
				crash.Speed + " | " +
				crash.TrackAngle + " | " +
				crash.DriftingAngle + " | " +
				crash.GameTick
			);
		}
	}
	#endregion

	#region protected instance methods
	protected static void Send(StreamWriter writer, SendMsg msg) {
		string json = msg.ToJson ();
		//Console.WriteLine (json);
		writer.WriteLine(json);
	}
	#endregion
}







