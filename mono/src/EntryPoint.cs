using System;
using System.IO;
using System.Text;
using System.Net.Sockets;
using System.Collections.Generic;
using AForge.Math.Random;
using AForge.Genetic;
using AForge;

public class EntryPoint
{
	#region protected class definitions
	protected class EvaluateChromosomes : IFitnessFunction{
		public IList<AI.GameTickInformation> raceInformation;

		public double Evaluate(IChromosome chromosome){
			SimpleVehiclePhysics vehicle = EntryPoint.ChromosomeToVehiclePhysics (chromosome);
			double error = 0.0;

			foreach (AI.GameTickInformation tick in this.raceInformation) {
				double estimatedSpeed = vehicle.CalculateFinalSpeed (tick.Throttle, tick.InitialSpeed, tick.Time);
				if (double.IsNaN (estimatedSpeed)) {
					error += 10000000.0;
				}else{
					error += Math.Min (100.0, Math.Abs(tick.FinalSpeed - estimatedSpeed));
				}

				double estimatedDrifting = vehicle.CalculateDrifting (
					tick.InitialSpeed, 
					tick.TrackRadius, 
					tick.InitialDrifting
				);

				if (double.IsNaN (estimatedDrifting)) {
					error += 10000000.0;
				}else{
					error += Math.Min (100.0, Math.Abs(tick.FinalDrifting - estimatedDrifting));
				}
			}

			double result = Math.Max (1000000.0 - error, 1.0);
			return result;
		}
	}
	#endregion

	#region protected static properties
	protected static IRandomNumberGenerator ChromosomeGenerator = 
	new UniformGenerator(new AForge.Range(-2f, 2f));

	protected static IRandomNumberGenerator MutationMultiplierGenerator = 
	new ExponentialGenerator( 1f );

	protected static IRandomNumberGenerator MutationAdditionGenerator = 
	new UniformGenerator( new AForge.Range( -0.5f, 0.5f ) );

	protected static ISelectionMethod ChromosomeSelectionMethod = 
	new EliteSelection( );

	protected static int PopulationSize = 1000;
	protected static int Iterations = 3;
	protected static double MutationRate = 0.3;
	protected static double CrossoverRate = 0.7;
	#endregion

	#region Application Entry Point
	public static void Main(string[] args) {
		string host = args [0];
		int port = int.Parse (args [1]);
		string botName = args [2];
		string botKey = args [3];
		bool machineLearning = args.Length >= 5 && string.Equals (args [4], "machine_learning");

		// Read the files
		IList<IChromosome> initialPopulation = EntryPoint.ReadVehiclePhysicsFile ();
		IList<AI.GameTickInformation> raceInformation = EntryPoint.ReadRaceFile ();

		// Add a default vehicle physics in case we don't find the file
		initialPopulation.Add ( EntryPoint.VehiclePhysicsToChromosome(
			new SimpleVehiclePhysics (1.05, 0.072, 0.4) //grip: 0.421, 0.425, 0.4251
		));

		//SimpleVehiclePhysics vehiclePhysics = new AI (EntryPoint.ChromosomeToVehiclePhysics (initialPopulation [initialPopulation.Count - 1]));
		SimpleVehiclePhysics vehiclePhysics = EntryPoint.ChromosomeToVehiclePhysics (initialPopulation [0]);
		vehiclePhysics.Grip = 0.4575;

		AI ai = new AI (vehiclePhysics, machineLearning);

		// Check if we want to race or apply machine learning
		if (machineLearning) {
			// If we don't have information about previous races, race at least one time
			if (raceInformation == null || raceInformation.Count == 0) {
				Console.WriteLine ("Racing File not found, racing at least one time");
				EntryPoint.ConnectToServer (ai, host, port, botName, botKey);
				raceInformation = ai.RaceHistory;

				EntryPoint.WriteRaceFile (raceInformation);
			}

			EntryPoint.MachineLearning (raceInformation, initialPopulation);
		}else{
			EntryPoint.ConnectToServer (ai, host, port, botName, botKey);
		}
	}
	#endregion

	#region protected class methods
	protected static void ConnectToServer(AI ai, string host, int port, string botName, string botKey){
		Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);
		using(TcpClient client = new TcpClient(host, port)) {
			NetworkStream stream = client.GetStream();
			StreamReader reader = new StreamReader(stream);
			StreamWriter writer = new StreamWriter(stream);
			writer.AutoFlush = true;

			new Bot(ai, reader, writer, new JoinMsg(botName, botKey));
		}
	}

	protected static void MachineLearning(
		IList<AI.GameTickInformation> raceInformation,
		IList<IChromosome> initialPopulation
	){
		EntryPoint.EvaluateChromosomes evaluation = new EntryPoint.EvaluateChromosomes ();
		evaluation.raceInformation = raceInformation;

		// Try to learn from the last race...
		EntryPoint.ReadMachineLearningParameters ();
		Population population = new Population (
			EntryPoint.PopulationSize,
			EntryPoint.CreateChromosome(),
			evaluation,
			EntryPoint.ChromosomeSelectionMethod
		);

		foreach (IChromosome chromosome in initialPopulation) {
			if (chromosome as DoubleArrayChromosome != null) {
				population.AddChromosome (chromosome);
			}
		}

		population.AutoShuffling = true;
		population.MutationRate = EntryPoint.MutationRate;
		population.CrossoverRate = EntryPoint.CrossoverRate;

		// Iterate as many times as desired...
		Console.WriteLine ("Machine Learning...");
		for (int i = 0; i < EntryPoint.Iterations; ++i) {
			// Run one epoch of genetic algorithm
			population.RunEpoch( );
			Console.WriteLine ("Best Chromosome (" + i + "): " + population.BestChromosome.Fitness);

			// Write the results to an external file
			List<IChromosome> chromosomes = new List<IChromosome> ();
			for (int j = 0; j < population.Size; ++j){
				IChromosome chromosome = population[j];
				if (chromosome != null && !chromosome.Equals (population.BestChromosome)) {
					chromosomes.Add (chromosome);
				}
			}

			EntryPoint.WriteVehiclePhysicsFile (population.BestChromosome, chromosomes);
		}
	}

	protected static void ReadMachineLearningParameters(){
		if (File.Exists ("resources/machine_learning.txt")) {
			string[] lines = File.ReadAllLines ("resources/machine_learning.txt");

			if (lines.Length >= 1) {
				EntryPoint.PopulationSize = int.Parse(lines[0]);

				if (lines.Length >= 2) {
					EntryPoint.Iterations = int.Parse(lines[1]);

					if (lines.Length >= 3) {
						EntryPoint.MutationRate = double.Parse(lines[2]);

						if (lines.Length >= 4) {
							EntryPoint.CrossoverRate = double.Parse(lines[3]);

							if (lines.Length >= 5) {
								int temp = int.Parse (lines [4]);

								if (temp == 0) {
									EntryPoint.ChromosomeSelectionMethod = new EliteSelection ();
								} else if (temp == 1) {
									EntryPoint.ChromosomeSelectionMethod = new RankSelection ();
								} else if (temp == 2) {
									EntryPoint.ChromosomeSelectionMethod = new RouletteWheelSelection ();
								}
							}
						}
					}
				}
			}
		}
	}

	protected static IList<AI.GameTickInformation> ReadRaceFile(){
		List<AI.GameTickInformation> race = new List<AI.GameTickInformation> ();

		if (File.Exists ("resources/race_information.txt")) {
			string[] lines = File.ReadAllLines ("resources/race_information.txt");

			foreach (string line in lines) {
				string[] parameters = line.Split (new char[]{ '\t' }, StringSplitOptions.RemoveEmptyEntries);

				try{
					race.Add(new AI.GameTickInformation(
						double.Parse(parameters[0]),
						double.Parse(parameters[1]),
						double.Parse(parameters[2]),
						double.Parse(parameters[3]),
						double.Parse(parameters[4]),
						double.Parse(parameters[5]),
						double.Parse(parameters[6])
					));
				}catch{}
			}
		}

		return race;
	}

	protected static IList<IChromosome> ReadVehiclePhysicsFile(){
		List<IChromosome> chromosomes = new List<IChromosome> ();

		if (File.Exists ("resources/vehicle_physics.txt")) {
			string[] lines = File.ReadAllLines ("resources/vehicle_physics.txt");

			foreach (string line in lines) {
				string[] parameters = line.Split (new char[]{ '\t' }, StringSplitOptions.RemoveEmptyEntries);

				try{
					DoubleArrayChromosome chromosome = EntryPoint.CreateChromosome();
					for (int i = 0; i < parameters.Length; ++i){
						chromosome.Value[i] = double.Parse(parameters[i]);
					}
					chromosomes.Add(chromosome);
				}catch{}
			}
		}

		return chromosomes;
	}

	protected static void WriteRaceFile(IList<AI.GameTickInformation> race){
		if (File.Exists ("resources/race_information.txt")) {
			// If the file already exist, create a backup
			File.WriteAllBytes (
				"resources/race_information.txt.backup", 
				File.ReadAllBytes ("resources/race_information.txt")
			);
		}

		StringBuilder sb = new StringBuilder ();
		char newLine = '\n';
		char tab = '\t';

		if (race != null){
			foreach (AI.GameTickInformation tick in race) {
				sb	.Append (tick.InitialSpeed)		.Append (tab)
					.Append (tick.InitialDrifting)	.Append (tab)
					.Append (tick.TrackRadius)		.Append (tab)
					.Append (tick.Throttle)			.Append (tab)
					.Append (tick.Time)				.Append (tab)
					.Append (tick.FinalSpeed)		.Append (tab)
					.Append (tick.FinalDrifting)	.Append (newLine);
			}
		}

		File.WriteAllText("resources/race_information.txt", sb.ToString());
	}

	protected static void WriteVehiclePhysicsFile(IChromosome best, IList<IChromosome> population){
		if (File.Exists ("resources/vehicle_physics.txt")) {
			// If the file already exist, create a backup
			File.WriteAllBytes(
				"resources/vehicle_physics.txt.backup", 
				File.ReadAllBytes ("resources/vehicle_physics.txt")
			);
		}

		DoubleArrayChromosome c = best as DoubleArrayChromosome;
		StringBuilder sb = new StringBuilder ();
		char newLine = '\n';
		char tab = '\t';

		if (c != null) {
			sb	.Append (c.Value [0]).Append (tab)
				.Append (c.Value [1]).Append (tab)
				.Append (c.Value [2]).Append (tab)
				.Append (c.Value [3]).Append (tab)
				.Append (c.Value [4]).Append (tab)
				.Append (c.Value [5]).Append (tab)
				.Append (c.Value [6]).Append (newLine);
		}

		if (population != null){
			foreach (IChromosome i in population) {
				c = i as DoubleArrayChromosome;
				if (c != null && !c.Equals(best)){
					sb	.Append (c.Value [0]).Append (tab)
						.Append (c.Value [1]).Append (tab)
						.Append (c.Value [2]).Append (tab)
						.Append (c.Value [3]).Append (tab)
						.Append (c.Value [4]).Append (tab)
						.Append (c.Value [5]).Append (tab)
						.Append (c.Value [6]).Append (newLine);
				}
			}
		}

		File.WriteAllText("resources/vehicle_physics.txt", sb.ToString());
	}

	protected static DoubleArrayChromosome CreateChromosome(){
		return new DoubleArrayChromosome (
			EntryPoint.ChromosomeGenerator,
			EntryPoint.MutationMultiplierGenerator,
			EntryPoint.MutationAdditionGenerator,
			7
		);
	}

	protected static SimpleVehiclePhysics ChromosomeToVehiclePhysics(IChromosome chromosome){
		DoubleArrayChromosome c = chromosome as DoubleArrayChromosome;
		if (c != null && c.Length >= 7){
			return new SimpleVehiclePhysics (
				c.Value[0],
				c.Value[1],
				c.Value[2],
				c.Value[3],
				c.Value[4],
				c.Value[5],
				c.Value[6]
			);
		}

		return null;
	}

	protected static IChromosome VehiclePhysicsToChromosome(SimpleVehiclePhysics vehiclePhysics){
		if (vehiclePhysics != null) {
			DoubleArrayChromosome chromosome = EntryPoint.CreateChromosome ();

			chromosome.Value [0] = vehiclePhysics.ConstantThrottleCoefficient;
			chromosome.Value [1] = vehiclePhysics.LinearThrottleCoefficient;
			chromosome.Value [2] = vehiclePhysics.ConstantFrictionCoefficient;
			chromosome.Value [3] = vehiclePhysics.LinearFrictionCoefficient;
			chromosome.Value [4] = vehiclePhysics.ConstantDriftingCoefficient;
			chromosome.Value [5] = vehiclePhysics.LinearDriftingCoefficient;
			chromosome.Value [6] = vehiclePhysics.Grip;

			return chromosome;
		}
		return null;
	}
	#endregion
}

