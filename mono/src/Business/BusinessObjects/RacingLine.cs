﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

public class RacingLine
{
	#region public instance properties
	public ReadOnlyCollection<Sector> Sectors { get; protected set;}
	public double TotalDistance { get; protected set; }
	public int TighestCorner { get; protected set; }
	#endregion

	#region public instance constructors
	public RacingLine(double distanceFromCenter, IEnumerable<TrackPieceDBO> pieces){
		this.TotalDistance = 0.0;
		this.TighestCorner = -1;

		Sector tighestCornerSector = null;
		int index = 0;

		List<Sector> s = new List<Sector>();
		if (pieces != null) {
			foreach (TrackPieceDBO piece in pieces){
				StraightPieceDBO straight = piece as StraightPieceDBO;
				Sector sector = null;

				if (straight != null) {
					sector = new Sector (straight.Length, straight.CanSwitchLanes, straight.IsBridge);
				} else {
					BendPieceDBO corner = piece as BendPieceDBO;
					if (corner != null) {
						int signcorner = Math.Sign(corner.Angle);

						sector = new Sector(
							corner.Radius - signcorner * distanceFromCenter,
							corner.Angle, 
							corner.CanSwitchLanes, 
							corner.IsBridge
						);

						// See if it's the tighest corner
						if (
							tighestCornerSector == null ||
							Math.Abs (tighestCornerSector.Radius) > Math.Abs (sector.Radius)
						) {
							this.TighestCorner = index;
							tighestCornerSector = sector;
						}
					}
				}

				if (sector != null) {
					this.TotalDistance += sector.Length;
					s.Add(sector);
				}

				++index;
			}
		}

		this.Sectors = new ReadOnlyCollection<Sector> (s);
	}
	#endregion

	#region public instance methods
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/// <summary>
	/// Gets the distance from the beginning of the first sector to the beginning of the second sector.
	/// </summary>
	/// <param name="startSectorIndex">Start sector index.</param>
	/// <param name="endSectorIndex">End sector index.</param>
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public virtual double GetDistance(int startSectorIndex, int endSectorIndex){
		int count = this.Sectors.Count;
		double distance = 0.0;

		// We could search a negative sector at the start of the race,
		// before the vehicle cross de start line.
		while (startSectorIndex < 0) {
			startSectorIndex += count;
			endSectorIndex += count;
		}

		// If the end sector is before the start sector, it's ok:
		// that only means we will need to cross the finish line
		// to calculate the desired distance
		if (endSectorIndex < startSectorIndex) {
			endSectorIndex += count;
		}

		// Now, iterate over the desired sectors and sum their distances
		for (int i = startSectorIndex; i < endSectorIndex; ++i) {
			distance += this.Sectors[i % count].Length;
		}

		return distance;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/// <summary>
	/// Gets the distance from the beginning of the first sector to the beginning of the second sector.
	/// </summary>
	/// <param name="startSectorIndex">Start sector index.</param>
	/// <param name="endSectorIndex">End sector index.</param>
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public virtual double GetEstimatedTime(int startSectorIndex, int endSectorIndex){
		int count = this.Sectors.Count;
		double time = 0.0;

		// We could search a negative sector at the start of the race,
		// before the vehicle cross de start line.
		while (startSectorIndex < 0) {
			startSectorIndex += count;
			endSectorIndex += count;
		}

		// If the end sector is before the start sector, it's ok:
		// that only means we will need to cross the finish line
		// to calculate the desired distance
		if (endSectorIndex < startSectorIndex) {
			endSectorIndex += count;
		}

		// Now, iterate over the desired sectors and sum their distances
		for (int i = startSectorIndex; i < endSectorIndex; ++i) {
			Sector sector = this.Sectors [i % count];
			time += sector.GetEstimatedTime ();
		}

		return time;
	}
	#endregion
}
