﻿using System;

public class Vehicle
{
	#region public instance properties
	public bool Crashed { get; set; }
	public bool Disqualfied { get; set; }

	public VehicleId Id {get; protected set;}
	public VehicleDimensionsDBO Dimensions { get; protected set; }
	public VehiclePositionDBO CurrentPosition { get; protected set; }
	public VehiclePositionDBO PreviousPosition { get; protected set; }

	public double TotalDistance { get; protected set; }
	public double CurrentSpeed { get; protected set; }
	public double PreviousSpeed { get; protected set; }
	#endregion

	#region protected instance fields
	protected Track _track;
	protected int _previousGameTick = -1;
	#endregion

	#region public instance constructor
	public Vehicle (VehicleId id , VehicleDimensionsDBO dimensions, Track track)
	{
		this.Id = id;
		this.Dimensions = dimensions;

		this._track = track;
	}
	#endregion

	#region public instance methods
	public virtual double GetSectorProgress(){
		Sector sector = this._track.GetSector (
			this.CurrentPosition.StartLane,
			this.CurrentPosition.CurrentPieceIndex
		);

		return this.CurrentPosition.CurrentPieceDistance / sector.Length;
	}

	public virtual void OnLapFinished(){

	}

	public virtual void OnRaceStarted(){
		this.TotalDistance = 0.0;
		this.CurrentSpeed = 0.0;
		this.PreviousSpeed = 0.0;
		this.Crashed = false;
		this.Disqualfied = false;
		this.CurrentPosition = null;
		this.PreviousPosition = null;
		this._previousGameTick = 0;
	}

	public virtual void OnRaceFinished(){

	}

	public virtual void Update(VehiclePositionDBO position, int gameTick){
		this.UpdateSpeed(this.PreviousPosition, position, (double)(gameTick - this._previousGameTick));

		this.CurrentPosition = position;
		this.PreviousPosition = this.CurrentPosition;
		this._previousGameTick = gameTick;
	}
	#endregion

	#region protected instance methods
	protected virtual void UpdateSpeed(
		VehiclePositionDBO previousPosition, 
		VehiclePositionDBO currentPosition,
		double dt
	){
		// TODO: improve this method to improve accuracy
		if (dt > 0.0) {
			double distance;

			if (previousPosition == null) {
				previousPosition = currentPosition;
			}

			double previousPieceDistance = previousPosition.CurrentPieceDistance;
			int previousPiece = previousPosition.CurrentPieceIndex;
			int previousLane = previousPosition.StartLane;

			double currentPieceDistance = currentPosition.CurrentPieceDistance;
			int currentPiece = currentPosition.CurrentPieceIndex;
			int currentLane = currentPosition.StartLane;

			double previousPieceLength = this._track.GetSector (previousLane, previousPiece).Length;
			//double previousPieceRadius = this._track.GetSector (previousLane, previousPiece).Radius;
			double previousPieceAngle = this._track.GetSector (previousLane, previousPiece).Angle;

			double currentPieceLength = this._track.GetSector (previousLane, currentPiece).Length;
			//double currentPieceRadius = this._track.GetSector (previousLane, currentPiece).Radius;
			double currentPieceAngle = this._track.GetSector (previousLane, currentPiece).Angle;

			// Check if the vehicle still continues in the same track piece
			if (previousPiece == currentPiece) {
				// In that case, check if the vehicle is switching lanes...
				if (previousLane != currentLane) {
					// In that case, estimate the travelled distance
					distance = currentPieceDistance - previousPieceDistance;
				} else {
					distance = currentPieceDistance - previousPieceDistance;
				}
			} else {
				// In that case, check if the vehicle is switching lanes...
				if (previousLane != currentLane) {
					// In that case, estimate the travelled distance
					distance = Math.Max(0.0, previousPieceLength - previousPieceDistance) + currentPieceDistance;
				} else {
					distance = Math.Max(0.0, previousPieceLength - previousPieceDistance) + currentPieceDistance;
				}
			}
			/*
			if (previousPieceLength < previousPieceDistance) {
				Console.WriteLine (
					previousPiece + "( " + previousPieceLength + ") --> " + 
					currentPiece + "( " + currentPieceLength + ") ||| " + 
					previousPieceDistance + " ---> " + currentPieceDistance + " ||| " +
					distance + " ____ " + previousPieceAngle
				);
			}
			*/
			this.PreviousSpeed = this.CurrentSpeed;
			this.CurrentSpeed = Math.Max(0.0, distance / dt);
			this.TotalDistance += distance;
		}
	}
	#endregion
}

