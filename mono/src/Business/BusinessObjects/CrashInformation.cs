﻿using System;


public class CrashInformation
{
	#region public instance properties
	public double Speed { get; protected set; }
	public double TrackAngle  { get; protected set; }
	public double DriftingAngle  { get; protected set; }
	public int GameTick  { get; protected set; }
	#endregion

	#region public instance constructors
	public CrashInformation(
		double speed,
		double trackAngle,
		double driftingAngle,
		int gameTick
	){
		this.Speed = speed;
		this.TrackAngle = trackAngle;
		this.DriftingAngle = driftingAngle;
		this.GameTick = gameTick;
	}
	#endregion
}