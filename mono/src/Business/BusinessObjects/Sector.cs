﻿using System;

public class Sector
{
	#region public instance properties
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/// <summary>
	/// Gets or sets the angle of the circular segment that the piece forms. For left turns, 
	/// this is a negative value. For a 45-degree bend to the left, the angle would be -45. 
	/// For a 30-degree turn to the right, this would be 30.
	/// https://helloworldopen.com/img/rules/bend.jpg
	/// </summary>
	/// <value>The angle.</value>
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public double Angle {get; protected set;}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/// <summary>
	/// Gets or sets a value indicating whether the vehicle can switch lanes in this piece.
	/// </summary>
	/// <value><c>true</c> if the vehicle can switch lanes; otherwise, <c>false</c>.</value>
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public bool CanSwitchLanes {get; protected set;}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/// <summary>
	/// Gets or sets a value indicating whether this instance is bridge.This attribute is merely a visualization 
	/// cue and does not affect the actual game.
	/// </summary>
	/// <value><c>true</c> if this instance is a bridge; otherwise, <c>false</c>.</value>
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public bool IsBridge {get; protected set;}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/// <summary>
	/// Gets or sets the length of the track piece.
	/// </summary>
	/// <value>The length.</value>
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public double Length {get; protected set;}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/// <summary>
	/// Gets or sets the radius of the circle that would be formed by this kind of pieces. 
	/// More specifically, this is the radius at the center line of the track.
	/// </summary>
	/// <value>The radius from the center of the track.</value>
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public double Radius {get; protected set;}
	#endregion

	#region private instance properties
	private double _estimatedSpeed = 0.0;
	private double _maxSpeed = 0.0;
	private double _grip = 1.0;
	private double _acceleration = 1.0;
	private double _deceleration = 1.0;
	#endregion

	#region public constructor
	public Sector (double length, bool canSwitchLanes, bool isBridge)
	{
		this.Length = length;
		this.CanSwitchLanes = canSwitchLanes;
		this.IsBridge = isBridge;

		this.Radius = double.PositiveInfinity;
		this.Angle = double.NaN;
	}

	public Sector (double radius, double angle, bool canSwitchLanes, bool isBridge)
	{
		this.Radius = radius;
		this.Angle = angle;
		this.CanSwitchLanes = canSwitchLanes;
		this.IsBridge = isBridge;

		this.Length = MathHelper.GetDistance(radius, angle);
	}
	#endregion

	#region public instance methods
	public virtual double GetAcceleration(){
		return this._acceleration;
	}

	public virtual double GetDeceleration(){
		return this._deceleration;
	}

	public virtual double GetEstimatedSpeed(){
		return this._estimatedSpeed;
	}

	public virtual double GetEstimatedTime(){
		return this.Length / this.GetEstimatedSpeed ();
	}

	public virtual double GetGrip(){
		return this._grip;
	}

	public virtual double GetMaxSpeed(){
		return this._maxSpeed;
	}

	public virtual void SetAcceleration(double acceleration){
		this._acceleration = acceleration;
	}

	public virtual void SetDeceleration(double deceleration){
		this._deceleration = deceleration;
	}

	public virtual void SetEstimatedSpeed(double estimatedSpeed){
		this._estimatedSpeed = estimatedSpeed;
	}

	public virtual void SetGrip(double grip){
		this._grip = grip;
	}

	public virtual void SetMaxSpeed(double maxSpeed){
		this._maxSpeed = Math.Max (0.0, maxSpeed);
	}
	#endregion
}

