﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

public class Track
{
	#region public instance properties
	public string Id { get; protected set; }
	public string Name { get; protected set; }

	public int SectorCount {
		get {
			return this.RacingLines [0].Sectors.Count;
		}
	}

	public ReadOnlyCollection<RacingLine> RacingLines { get; protected set; }
	public ReadOnlyCollection<int> SwitchLaneSectors { get; protected set; }
	#endregion

	#region public instance constructor
	public Track (TrackDBO track)
	{
		if (track != null) {
			this.Id = track.Id;
			this.Name = track.Name;
			this.RacingLines = this.ReadRacingLines (track);
			this.SwitchLaneSectors = this.ReadSwitchLaneSectors (track);

			// TODO: calculate the length of the racing line switching lines
		}
	}
	#endregion

	#region public instance methods
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/// <summary>
	/// Gets the distance from the beginning of the first sector to the beginning of the second sector 
	/// using the specified lane during all the path.
	/// </summary>
	/// <param name="startSectorIndex">Start sector index.</param>
	/// <param name="endSectorIndex">End sector index.</param>
	/// <param name="laneIndex">Lane index.</param>
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public virtual double GetDistance(
		int startSectorIndex, 
		int endSectorIndex,
		int laneIndex
	){
		return this.RacingLines [laneIndex].GetDistance (startSectorIndex, endSectorIndex);
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/// <summary>
	/// Gets the distance from the beginning of the first sector to the beginning of the second sector, 
	/// starting from the specified lane and applying the "Switch Lane" commands in the defined order.
	/// If one "Switch Lane" command appears to be out of order, that mean that it should be applied 
	/// in the next lap.
	/// </summary>
	/// <param name="startSectorIndex">Start sector index.</param>
	/// <param name="endSectorIndex">End sector index.</param>
	/// <param name="startLaneIndex">Start lane index.</param>
	/// <param name="switchLaneCommands">"Switch Lane" commands.</param>
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public virtual double GetDistance(
		int startSectorIndex, 
		int endSectorIndex,
		int startLaneIndex,
		IList<SwitchLaneCommand> switchLaneCommands 
	){
		int count = this.SectorCount;
		int nextSwitchIndex = 0;
		double distance = 0.0;

		if (switchLaneCommands == null) {
			switchLaneCommands = new SwitchLaneCommand[0];
		}

		// We could search a negative sector at the start of the race,
		// before the vehicle cross de start line.
		while (startSectorIndex < 0) {
			startSectorIndex += count;
			endSectorIndex += count;
		}

		// If the end sector is before the start sector, it's ok:
		// that only means we will need to cross the finish line
		// to calculate the desired distance
		if (endSectorIndex < startSectorIndex) {
			endSectorIndex += count;
		}

		// Now, iterate over the desired sectors and sum their distances,
		// taking into account any lane switch
		for (int i = startSectorIndex; i < endSectorIndex; ++i) {
			int index = i % count;

			// Check if we have reached the next lane switch...
			if (
				nextSwitchIndex < switchLaneCommands.Count && 
				index == switchLaneCommands [nextSwitchIndex].SectorIndex
			) {
				// In that case, estimate the lenght of the sector taking
				// into account the lane change
				int newLaneIndex = this.RacingLineAfterSwitchLane (
					startLaneIndex, 
					switchLaneCommands [nextSwitchIndex].Direction
				);

				// Check if the "Switch Lane" command is valid
				if (newLaneIndex != startLaneIndex) {
					// In that case, calculate the length of the path while changing lanes
					distance += this.GetDistanceSwitchingLanes (
						this.RacingLines[startLaneIndex].Sectors[index],
						this.RacingLines[newLaneIndex].Sectors[index]
					);
				} else {
					// Otherwise; sum the length of the sector in the current lane
					distance += this.RacingLines[startLaneIndex].Sectors[index].Length;
				}

				startLaneIndex = newLaneIndex;
				++nextSwitchIndex;
			} else {
				// Otherwise; sum the length of the sector in the current lane
				distance += this.RacingLines[startLaneIndex].Sectors[index].Length;
			}
		}

		return distance;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/// <summary>
	/// Gets the estimated time from the beginning of the first sector to the beginning of the second sector 
	/// using the specified lane during all the path.
	/// </summary>
	/// <param name="startSectorIndex">Start sector index.</param>
	/// <param name="endSectorIndex">End sector index.</param>
	/// <param name="laneIndex">Lane index.</param>
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public virtual double GetEstimatedTime(
		int startSectorIndex, 
		int endSectorIndex,
		int laneIndex
	){
		return this.RacingLines [laneIndex].GetEstimatedTime (startSectorIndex, endSectorIndex);
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/// <summary>
	/// Gets the estimated time from the beginning of the first sector to the beginning of the second sector, 
	/// starting from the specified lane and applying the "Switch Lane" commands in the defined order.
	/// If one "Switch Lane" command appears to be out of order, that mean that it should be applied 
	/// in the next lap.
	/// </summary>
	/// <param name="startSectorIndex">Start sector index.</param>
	/// <param name="endSectorIndex">End sector index.</param>
	/// <param name="startLaneIndex">Start lane index.</param>
	/// <param name="switchLaneCommands">"Switch Lane" commands.</param>
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public virtual double GetEstimatedTime(
		int startSectorIndex, 
		int endSectorIndex,
		int startLaneIndex,
		IList<SwitchLaneCommand> switchLaneCommands 
	){
		int count = this.SectorCount;
		int nextSwitchIndex = 0;
		double time = 0.0;

		if (switchLaneCommands == null) {
			switchLaneCommands = new SwitchLaneCommand[0];
		}

		// We could search a negative sector at the start of the race,
		// before the vehicle cross de start line.
		while (startSectorIndex < 0) {
			startSectorIndex += count;
			endSectorIndex += count;
		}

		// If the end sector is before the start sector, it's ok:
		// that only means we will need to cross the finish line
		// to calculate the desired distance
		if (endSectorIndex < startSectorIndex) {
			endSectorIndex += count;
		}

		// Now, iterate over the desired sectors and sum their distances,
		// taking into account any lane switch
		for (int i = startSectorIndex; i < endSectorIndex; ++i) {
			int index = i % count;

			// Check if we have reached the next lane switch...
			if (
				nextSwitchIndex < switchLaneCommands.Count && 
				index == switchLaneCommands [nextSwitchIndex].SectorIndex
			) {
				// In that case, estimate the lenght of the sector taking
				// into account the lane change
				int newLaneIndex = this.RacingLineAfterSwitchLane (
					startLaneIndex, 
					switchLaneCommands [nextSwitchIndex].Direction
				);

				// Check if the "Switch Lane" command is valid
				if (newLaneIndex != startLaneIndex) {
					// In that case, calculate the length of the path while changing lanes
					time += this.GetEstimatedTimeSwitchingLanes (
						this.RacingLines[startLaneIndex].Sectors[index],
						this.RacingLines[newLaneIndex].Sectors[index]
					);
				} else {
					// Otherwise; sum the length of the sector in the current lane
					time += this.RacingLines [startLaneIndex].Sectors [index].GetEstimatedTime ();
				}

				startLaneIndex = newLaneIndex;
				++nextSwitchIndex;
			} else {
				// Otherwise; sum the length of the sector in the current lane
				time += this.RacingLines [startLaneIndex].Sectors [index].GetEstimatedTime ();
			}
		}

		return time;
	}

	public Sector GetSector(int racingLineIndex, int sectorIndex){
		return this.RacingLines [racingLineIndex].Sectors[sectorIndex];
	}
	#endregion

	#region protected instance methods
	protected virtual double GetDistanceSwitchingLanes(Sector startLane, Sector endLane){
		// TODO: improve this method to improve accuracy
		return 0.75 * (startLane.Length + endLane.Length);
	}

	protected virtual double GetEstimatedTimeSwitchingLanes(Sector startLane, Sector endLane){
		// TODO: improve this method to improve accuracy
		return 0.75 * (startLane.GetEstimatedTime() + endLane.GetEstimatedTime());
	}

	protected virtual int RacingLineAfterSwitchLane(int index, SwitchLaneDirection direction){
		if (direction == SwitchLaneDirection.Left) {
			return Math.Max (0, index - 1);
		} else {
			return Math.Min (this.RacingLines.Count - 1, index + 1);
		}
	}

	protected virtual ReadOnlyCollection<RacingLine> ReadRacingLines(TrackDBO track){
		if (track.Lanes != null) {
			RacingLine[] racingLines = new RacingLine[track.Lanes.Count];

			foreach (LaneDBO lane in track.Lanes) {
				racingLines [lane.Index] = new RacingLine (lane.DistanceFromCenter, track.Pieces);
			}

			return new ReadOnlyCollection<RacingLine>(racingLines);
		} else {
			return new ReadOnlyCollection<RacingLine>(new RacingLine[0]);
		}
	}

	protected virtual ReadOnlyCollection<int> ReadSwitchLaneSectors(TrackDBO track){
		List<int> sectors = new List<int> ();

		if (track.Lanes != null) {
			for (int i = 0; i < track.Pieces.Count; ++i){
				TrackPieceDBO piece = track.Pieces [i];

				if (piece != null && piece.CanSwitchLanes) {
					sectors.Add (i);
				}
			}

		}

		return new ReadOnlyCollection<int> (sectors);
	}
	#endregion
}
