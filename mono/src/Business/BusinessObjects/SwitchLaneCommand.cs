﻿using System;


public class SwitchLaneCommand{
	#region public instance properties
	public int SectorIndex { get; protected set; }
	public SwitchLaneDirection Direction { get; protected set; }
	#endregion

	#region public instance constructor
	public SwitchLaneCommand (int sectorIndex, SwitchLaneDirection direction)
	{
		this.SectorIndex = sectorIndex;
		this.Direction = direction;
	}
	#endregion
}

