﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

public abstract class Manager<TKey, TValue>
{
	#region public instance properties
	public int Count { get { return this._cache.Count; } }
	#endregion

	#region protected instance properties
	protected Dictionary<TKey, TValue> _cache;
	#endregion

	#region protected instance constructor
	protected Manager ()
	{
		this._cache = new Dictionary<TKey, TValue> ();
	}
	#endregion

	#region public instance methods
	public virtual void Set(TKey id, TValue value){
		this._cache [id] = value;
	}

	public virtual ReadOnlyCollection<TValue> GetAll(){
		return new ReadOnlyCollection<TValue> (this._cache.Values.ToArray ());
	}

	public virtual TValue Get(TKey id){
		TValue value;

		if (this._cache.TryGetValue (id, out value)) {
			return value;
		}

		return default(TValue);
	}

	public virtual TValue Remove(TKey id){
		TValue value;

		if (this._cache.TryGetValue (id, out value)) {
			this._cache.Remove (id);
			return value;
		}

		return default(TValue);
	}
	#endregion
}

