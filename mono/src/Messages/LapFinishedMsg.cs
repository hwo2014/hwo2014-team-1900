﻿using System;
using Newtonsoft.Json.Linq;

public class LapFinishedMsg : Msg
{
	#region public instance fields
	public VehicleId VehicleId { get; protected set; }

	public int Lap { get; protected set; }
	public int LapTime { get; protected set; }
	public int LapTicks { get; protected set; }

	public int RaceLaps { get; protected set; }
	public int RaceTime { get; protected set; }
	public int RaceTicks { get; protected set; }

	public int GlobalRanking { get; protected set; }
	public int FastestLapRanking { get; protected set; }

	public string GameId;
	public int GameTick;
	#endregion

	#region protected instance properties
	protected override string MessageType{ get{ return MsgType.LapFinished; } }
	#endregion

	#region public instance constructors
	public LapFinishedMsg(JObject json, string gameId, int gameTick) : this(
		new VehicleId(
			json ["car"] ["name"].ToString(), 
			json ["car"] ["color"].ToString()
		),
		int.Parse(json["lapTime"]["lap"].ToString()),
		int.Parse(json["lapTime"]["millis"].ToString()),
		int.Parse(json["lapTime"]["ticks"].ToString()),
		int.Parse(json["raceTime"]["laps"].ToString()),
		int.Parse(json["raceTime"]["millis"].ToString()),
		int.Parse(json["raceTime"]["ticks"].ToString()),
		int.Parse(json["ranking"]["overall"].ToString()),
		int.Parse(json["ranking"]["fastestLap"].ToString()),
		gameId,
		gameTick
	){}

	public LapFinishedMsg(
		VehicleId vehicleId,
		int lap,
		int lapTime,
		int lapTicks,
		int raceLaps,
		int raceTime,
		int raceTicks,
		int globalRanking,
		int fastestLapRanking,
		string gameId,
		int gameTick
	){
		this.VehicleId = vehicleId;
		this.Lap = lap;
		this.LapTime = lapTime;
		this.LapTicks = lapTicks;
		this.RaceLaps = raceLaps;
		this.RaceTime = raceTime;
		this.RaceTicks = raceTicks;
		this.GlobalRanking = globalRanking;
		this.FastestLapRanking = fastestLapRanking;
		this.GameId = gameId;
		this.GameTick = gameTick;
	}
	#endregion
}