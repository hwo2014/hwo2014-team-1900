﻿public class GameStartedMsg : Msg
{
	#region protected instance properties
	protected override string MessageType{ get{ return MsgType.GameStarted; } }
	#endregion
}