﻿using System;
using Newtonsoft.Json.Linq;

public class JoinedMsg : Msg
{
	#region public instance fields
	public VehicleId Id { get; protected set; }
	#endregion

	#region protected instance properties
	protected override string MessageType{ get{ return MsgType.Joined; } }
	#endregion

	#region public instance constructors
	public JoinedMsg(JObject json) : this(json ["name"].ToString(), json ["color"].ToString()){}
	public JoinedMsg(string name, string color) : this(new VehicleId(name, color)){}
	public JoinedMsg(VehicleId id)
	{
		this.Id = id;
	}
	#endregion
}