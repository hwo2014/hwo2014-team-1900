﻿public class TournamentFinishedMsg: Msg
{
	#region protected instance properties
	protected override string MessageType{ get{ return MsgType.TournamentFinished; } }
	#endregion
}