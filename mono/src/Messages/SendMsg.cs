﻿using Newtonsoft.Json;

public abstract class SendMsg : Msg{
	#region public instance methods
	public string ToJson() {
		return new MsgWrapper(this.MessageType, this.MsgData()).ToJson();
	}
	#endregion
}