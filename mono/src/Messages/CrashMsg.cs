﻿using System;
using Newtonsoft.Json.Linq;

public class CrashMsg : Msg
{
	#region public instance fields
	public VehicleId VehicleId { get; protected set; }
	public string GameId { get; protected set; }
	public int GameTick { get; protected set; }
	#endregion

	#region protected instance properties
	protected override string MessageType{ get{ return MsgType.Crash; } }
	#endregion

	#region public instance constructors
	public CrashMsg(JObject json, string gameId, int gameTick) : this(
		new VehicleId(
			json ["name"].ToString(), 
			json ["color"].ToString()
		),
		gameId,
		gameTick
	){}

	public CrashMsg(VehicleId vehicleId, string gameId, int gameTick)
	{
		this.VehicleId = vehicleId;
		this.GameId = gameId;
		this.GameTick = gameTick;
	}
	#endregion
}