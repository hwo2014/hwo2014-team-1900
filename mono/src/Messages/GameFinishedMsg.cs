﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;

public class GameFinishedMsg : Msg
{
	#region public class definitions
	public class VehicleRaceInformation
	{
		public VehicleId VehicleId { get; protected set; }
		public int RaceLaps { get; protected set; }
		public int RaceTime { get; protected set; }
		public int RaceTicks { get; protected set; }

		public VehicleRaceInformation(){}
		public VehicleRaceInformation(
			VehicleId vehicleId,
			int raceLaps,
			int raceTime,
			int raceTicks
		){
			this.VehicleId = vehicleId;
			this.RaceLaps = raceLaps;
			this.RaceTime = raceTime;
			this.RaceTicks = raceTicks;
		}
	}

	public class VehicleBestLap
	{
		public VehicleId VehicleId { get; protected set; }
		public int Lap { get; protected set; }
		public int LapTime { get; protected set; }
		public int LapTicks { get; protected set; }

		public VehicleBestLap(){}
		public VehicleBestLap(
			VehicleId vehicleId,
			int lap,
			int lapTime,
			int lapTicks
		){
			this.VehicleId = vehicleId;
			this.Lap = lap;
			this.LapTime = lapTime;
			this.LapTicks = lapTicks;
		}
	}
	#endregion

	#region public instance properties
	public List<VehicleRaceInformation> Results {
		get{ return this.__results; }
	}

	public List<VehicleBestLap> BestLaps {
		get{ return this.__bestLaps; }
	}
	#endregion

	#region protected instance properties
	protected override string MessageType{ get{ return MsgType.GameFinished; } }
	#endregion

	#region private instance fields
	private List<VehicleRaceInformation> __results = new List<VehicleRaceInformation>();
	private List<VehicleBestLap> __bestLaps = new List<VehicleBestLap>();
	#endregion

	#region public instance constructors
	public GameFinishedMsg(JObject json) : this(
		GameFinishedMsg.ReadResults(json["results"] as JArray),
		GameFinishedMsg.ReadBestLaps(json["bestLaps"] as JArray)
	){}

	public GameFinishedMsg(
		IEnumerable<VehicleRaceInformation> results,
		IEnumerable<VehicleBestLap> bestLaps
	){
		if (results != null) {
			foreach (VehicleRaceInformation result in results) {
				if (result != null) {
					this.__results.Add (result);
				}
			}
		}

		if (bestLaps != null) {
			foreach (VehicleBestLap bestLap in bestLaps) {
				if (bestLap != null) {
					this.__bestLaps.Add (bestLap);
				}
			}
		}
	}
	#endregion

	#region private class methods
	private static List<VehicleRaceInformation> ReadResults(JArray json){
		List<VehicleRaceInformation> list = new List<VehicleRaceInformation> ();

		if (json != null) {
			for (int i = 0; i < json.Count; ++i) {
				JObject result = json [i] as JObject;

				if (result != null) {
					JObject raceInfo = null;
					JToken temp;

					if (result.TryGetValue("result", out temp)){
						raceInfo = temp as JObject;
					}

					list.Add (new VehicleRaceInformation(
						new VehicleId(
							result["car"]["name"].ToString(),
							result["car"]["color"].ToString()
						),
						raceInfo != null ? int.Parse(raceInfo["laps"].ToString())	: -1,
						raceInfo != null ? int.Parse(raceInfo["millis"].ToString())	: -1,
						raceInfo != null ? int.Parse(raceInfo["ticks"].ToString())	: -1
					));
				}
			}
		}

		return list;
	}

	private static List<VehicleBestLap> ReadBestLaps(JArray json){
		List<VehicleBestLap> list = new List<VehicleBestLap> ();

		if (json != null) {
			for (int i = 0; i < json.Count; ++i) {
				JObject result = json [i] as JObject;
				if (result != null) {
					JObject bestLap = null;
					JToken temp;

					if (result.TryGetValue("result", out temp)){
						bestLap = temp as JObject;
					}

					list.Add (new VehicleBestLap(
						new VehicleId(
							result["car"]["name"].ToString(),
							result["car"]["color"].ToString()
						),
						bestLap != null ? int.Parse(bestLap["lap"].ToString())		: -1,
						bestLap != null ? int.Parse(bestLap["millis"].ToString())	: -1,
						bestLap != null ? int.Parse(bestLap["ticks"].ToString())	: -1
					));
				}
			}
		}

		return list;
	}
	#endregion
}