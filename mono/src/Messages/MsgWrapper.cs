﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

public class MsgWrapper {
	public string MsgType { get; protected set; }
	public object Data { get; protected set; }
	public int? GameTick { get; protected set; }

	public MsgWrapper(string msgType, object data){
		this.MsgType = msgType;
		this.Data = data;
	}

	public MsgWrapper(string msgType, object data, int gameTick) : this(msgType, data)
	{
		this.GameTick = gameTick;
	}

	public virtual string ToJson(){
		JObject json = new JObject ();
		json ["msgType"] = this.MsgType;

		if (this.Data == null) {
			json ["data"] = null;
		}else if (this.Data is JToken) {
			json ["data"] = (JToken) this.Data;
		} else if (this.Data is int) {
			json ["data"] = (int)this.Data;
		} else if (this.Data is double) {
			json ["data"] = (double)this.Data;
		}else{
			json ["data"] = this.Data.ToString();
		}

		if (this.GameTick != null) {
			json ["gameTick"] = this.GameTick;
		}

		return json.ToString(Formatting.None);
	}
}