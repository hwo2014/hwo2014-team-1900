﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;

public class GameInitializedMsg : Msg
{
	#region public class definitions
	public class VehicleInformation
	{
		public VehicleId Id { get; protected set; }
		public VehicleDimensionsDBO Dimensions { get; protected set; }

		public VehicleInformation(){}
		public VehicleInformation(VehicleId id, VehicleDimensionsDBO dimensions){
			this.Id = id;
			this.Dimensions = dimensions;
		}
	}
	#endregion

	#region public instance properties
	public TrackDBO Track { get; set; }
	public int Laps { get; set; }
	public int MaxLapTime { get; set; }
	public bool QuickRace { get; set; }
	public List<VehicleInformation> Vehicles{ get { return this.__vehicles; } }
	#endregion

	#region protected instance properties
	protected override string MessageType{ get{ return MsgType.GameInitialized; } }
	#endregion

	#region private instance fields
	private List<VehicleInformation> __vehicles = new List<VehicleInformation>();
	#endregion

	#region public instance constructors
	public GameInitializedMsg(JObject json) : this(
		GameInitializedMsg.ReadTrack(json["race"]["track"] as JObject),
		int.Parse(json["race"]["raceSession"]["laps"].ToString()),
		int.Parse(json["race"]["raceSession"]["maxLapTimeMs"].ToString()),
		bool.Parse(json["race"]["raceSession"]["quickRace"].ToString()),
		GameInitializedMsg.ReadVehicles(json["race"]["cars"] as JArray)
	){}

	public GameInitializedMsg(
		TrackDBO track,
		int laps,
		int maxLapTime,
		bool quickRace,
		IEnumerable<VehicleInformation> vehicles
	){
		this.Track = track;
		this.Laps = laps;
		this.MaxLapTime = maxLapTime;
		this.QuickRace = quickRace;

		if (vehicles != null) {
			foreach (VehicleInformation vehicle in vehicles) {
				if (vehicle != null) {
					this.__vehicles.Add (vehicle);
				}
			}
		}
	}
	#endregion

	#region private class methods
	private static List<VehicleInformation> ReadVehicles(JArray json){
		List<VehicleInformation> list = new List<VehicleInformation> ();

		if (json != null) {
			for (int i = 0; i < json.Count; ++i) {
				JObject vehicle = json [i] as JObject;
				if (vehicle != null) {
					list.Add(
						new VehicleInformation(
							new VehicleId(
								vehicle["id"]["name"].ToString(),
								vehicle["id"]["color"].ToString()
							),
							new VehicleDimensionsDBO(
								double.Parse(vehicle["dimensions"]["length"].ToString()),
								double.Parse(vehicle["dimensions"]["width"].ToString()),
								double.Parse(vehicle["dimensions"]["guideFlagPosition"].ToString())
							)
						)
					);
				}
			}
		}

		return list;
	}

	private static TrackDBO ReadTrack(JObject json){
		List<TrackPieceDBO> pieces = new List<TrackPieceDBO> ();
		List<LaneDBO> lanes = new List<LaneDBO> ();
		JToken temp;

		JArray array = json ["pieces"] as JArray;
		if (array != null) {
			for (int i = 0; i < array.Count; ++i) {
				JObject obj = array [i] as JObject;

				// If the piece is defined...
				if (obj != null) {
					bool canSwitchLane = false;
					bool isBridge = false;

					if (obj.TryGetValue ("switch", out temp) && temp != null) {
						canSwitchLane = bool.Parse(temp.ToString());
					}

					if (obj.TryGetValue ("bridge", out temp) && temp != null) {
						isBridge = bool.Parse(temp.ToString());
					}

					// Check if it's a bend piece or a straight piece
					if (obj.TryGetValue ("radius", out temp) && temp != null) {
						// If it's a bend piece...
						double radius = double.Parse (temp.ToString ());
						double angle = double.Parse (obj ["angle"].ToString ());

						pieces.Add( new BendPieceDBO(
							radius,
							angle,
							canSwitchLane,
							isBridge
						));
					} else {
						// If it's a straight piece...
						double length = double.Parse (obj ["length"].ToString ());
						pieces.Add( new StraightPieceDBO(
							length,
							canSwitchLane,
							isBridge
						));
					}
				}
			}
		}

		array = json ["lanes"] as JArray;
		if (array != null) {
			for (int i = 0; i < array.Count; ++i) {
				JObject obj = array [i] as JObject;

				if (obj != null) {
					lanes.Add (new LaneDBO (
						int.Parse(obj["index"].ToString()),
						double.Parse(obj["distanceFromCenter"].ToString())
					));
				}
			}
		}

		return new TrackDBO (
			json["id"].ToString(),
			json["name"].ToString(),
			double.Parse(json["startingPoint"]["angle"].ToString()),
			double.Parse(json["startingPoint"]["position"]["x"].ToString()),
			double.Parse(json["startingPoint"]["position"]["y"].ToString()),
			pieces,
			lanes
		);
	}
	#endregion
}