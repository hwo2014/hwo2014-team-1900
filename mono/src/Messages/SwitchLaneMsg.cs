﻿class SwitchLaneMsg: SendMsg {
	#region public class properties
	public static readonly string LeftDirection = "Left";
	public static readonly string RightDirection = "Right";
	#endregion

	#region public instance fields
	public SwitchLaneDirection Direction { get; protected set; }
	#endregion

	#region protected instance properties
	protected override string MessageType{ get{ return MsgType.SwitchLane; } }
	#endregion

	#region public instance constructor
	public SwitchLaneMsg(SwitchLaneDirection direction) {
		this.Direction = direction;
	}
	#endregion

	#region protected methods
	protected override object MsgData() {
		return this.Direction == SwitchLaneDirection.Left ? LeftDirection : RightDirection ;
	}
	#endregion
}