﻿using System;
using Newtonsoft.Json.Linq;

public class RaceFinishedMsg : Msg
{
	#region public instance fields
	public VehicleId VehicleId { get; protected set; }
	public string GameId;
	public int GameTick;
	#endregion

	#region protected instance properties
	protected override string MessageType{ get{ return MsgType.RaceFinished; } }
	#endregion

	#region public instance constructors
	public RaceFinishedMsg(JObject json, string gameId, int gameTick) : this(
		new VehicleId(
			json ["name"].ToString(), 
			json ["color"].ToString()
		),
		gameId,
		gameTick
	){}

	public RaceFinishedMsg(VehicleId vehicleId, string gameId, int gameTick)
	{
		this.VehicleId = vehicleId;
		this.GameId = gameId;
		this.GameTick = gameTick;
	}
	#endregion
}