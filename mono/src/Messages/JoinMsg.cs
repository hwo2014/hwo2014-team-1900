﻿using Newtonsoft.Json.Linq;

public class JoinMsg: SendMsg {
	#region public instance fields
	public VehicleId Id { get; protected set; }
	public string Key { get; protected set; }
	#endregion

	#region protected instance properties
	protected override string MessageType{ get{ return MsgType.Join; } }
	#endregion

	#region public instance constructors
	public JoinMsg(JObject json) : this(json["key"].ToString(), json["name"].ToString(), json["color"].ToString()){}
	public JoinMsg (string key, string name) : this (key, name, null){}
	public JoinMsg(string key, string name, string color) : this(key, new VehicleId(name, color)){}
	public JoinMsg(string key, VehicleId id) {
		this.Key = key;
		this.Id = id;
	}
	#endregion

	#region protected override methods
	protected override object MsgData (){
		JObject json = new JObject ();
		json ["key"] = this.Key;
		json ["name"] = this.Id.Name;

		if (!string.IsNullOrEmpty (this.Id.Color)) {
			json ["color"] = this.Id.Color;
		}

		return json;
	}
	#endregion
}