﻿using System;
using Newtonsoft.Json.Linq;

public class CreateRaceMsg : SendMsg
{
	#region public instance fields
	public string Name { get; protected set; }
	public string Key { get; protected set; }
	public string TrackName { get; protected set; }
	public string Password { get; protected set; }
	public int CarCount { get; protected set; }
	#endregion

	#region protected instance properties
	protected override string MessageType{ get{ return MsgType.CreateRace; } }
	#endregion

	#region public instance constructors
	public CreateRaceMsg(string name, string key, string trackName, string password, int carCount)
	{
		this.Name = name;
		this.Key = key;
		this.TrackName = trackName;
		this.Password = password;
		this.CarCount = carCount;
	}
	#endregion

	#region protected override methods
	protected override object MsgData (){
		JObject json = new JObject ();
		json ["botId"] = new JObject ();
		json ["botId"] ["name"] = this.Name;
		json ["botId"] ["key"] = this.Key;
		json ["trackName"] = this.TrackName;
		json ["password"] = this.Password;
		json ["carCount"] = this.CarCount;

		return json;
	}
	#endregion
}