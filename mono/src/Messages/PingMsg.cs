﻿class PingMsg: SendMsg {
	#region protected instance properties
	protected override string MessageType{ get{ return MsgType.Ping; } }
	#endregion

	#region protected override methods
	protected override object MsgData (){
		return null;
	}
	#endregion
}