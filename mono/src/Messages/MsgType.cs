public static class MsgType
{
	public static readonly string CarPositions			= "carPositions";
	public static readonly string Crash					= "crash";
	public static readonly string CreateRace			= "createRace";
	public static readonly string Disqualified			= "dnf";
	public static readonly string Error					= "error";
	public static readonly string GameFinished			= "gameEnd";
	public static readonly string GameInitialized		= "gameInit";
	public static readonly string GameStarted			= "gameStart";
	public static readonly string Join					= "join";
	public static readonly string JoinRace				= "joinRace";
	public static readonly string Joined				= "yourCar";
	public static readonly string LapFinished			= "lapFinished";
	public static readonly string Ping					= "ping";
	public static readonly string RaceFinished			= "finish";
	public static readonly string Spawn					= "spawn";
	public static readonly string SwitchLane			= "switchLane";
	public static readonly string Throttle				= "throttle";
	public static readonly string TournamentFinished	= "tournamentEnd";
}

