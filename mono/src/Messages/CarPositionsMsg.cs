﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;

public class CarPositionsMsg : Msg
{
	#region public class definitions
	public class VehicleInformation
	{
		public VehicleId Id { get; protected set; }
		public VehiclePositionDBO Position { get; protected set; }

		public VehicleInformation(){}
		public VehicleInformation(VehicleId id, VehiclePositionDBO position){
			this.Id = id;
			this.Position = position;
		}
	}
	#endregion

	#region public instance properties
	public List<VehicleInformation> Vehicles{ get { return this.__vehicles; } }
	public string GameId;
	public int GameTick;
	#endregion

	#region protected instance properties
	protected override string MessageType{ get{ return MsgType.CarPositions; } }
	#endregion

	#region private instance fields
	private List<VehicleInformation> __vehicles = new List<VehicleInformation>();
	#endregion

	#region public instance constructors
	public CarPositionsMsg(JArray json, string gameId, int gameTick) : 
	this(CarPositionsMsg.ReadVehicles(json), gameId, gameTick){}

	public CarPositionsMsg(IEnumerable<VehicleInformation> vehicles, string gameId, int gameTick){
		if (vehicles != null) {
			foreach (VehicleInformation vehicle in vehicles) {
				if (vehicle != null) {
					this.__vehicles.Add (vehicle);
				}
			}
		}

		this.GameId = gameId;
		this.GameTick = gameTick;
	}
	#endregion

	#region private class methods
	private static List<VehicleInformation> ReadVehicles(JArray json){
		List<VehicleInformation> list = new List<VehicleInformation> ();

		if (json != null) {
			for (int i = 0; i < json.Count; ++i) {
				JObject vehicle = json [i] as JObject;
				if (vehicle != null) {
					list.Add(
						new VehicleInformation(
							new VehicleId(
								vehicle["id"]["name"].ToString(),
								vehicle["id"]["color"].ToString()
							)
							,
							new VehiclePositionDBO(
								int.Parse(vehicle["piecePosition"]["lap"].ToString()),
								double.Parse(vehicle["angle"].ToString()),
								int.Parse(vehicle["piecePosition"]["pieceIndex"].ToString()),
								double.Parse(vehicle["piecePosition"]["inPieceDistance"].ToString()),
								int.Parse(vehicle["piecePosition"]["lane"]["startLaneIndex"].ToString()),
								int.Parse(vehicle["piecePosition"]["lane"]["endLaneIndex"].ToString())
							)
						)
					);
				}
			}
		}

		return list;
	}
	#endregion
}