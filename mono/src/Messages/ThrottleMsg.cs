﻿class ThrottleMsg: SendMsg {
	#region public instance fields
	public double Value { get; protected set; }
	#endregion

	#region protected instance properties
	protected override string MessageType{ get{ return MsgType.Throttle; } }
	#endregion

	#region public instance constructor
	public ThrottleMsg(double value) {
		this.Value = value;
	}
	#endregion

	#region protected methods
	protected override object MsgData() {
		return this.Value;
	}
	#endregion
}