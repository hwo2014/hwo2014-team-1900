﻿using System;

public abstract class Msg
{
	#region protected instance properties
	protected abstract string MessageType{ get; }
	#endregion

	#region protected instance methods
	protected virtual object MsgData() {
		return this;
	}
	#endregion
}

