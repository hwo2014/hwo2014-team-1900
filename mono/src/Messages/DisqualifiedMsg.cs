﻿using System;
using Newtonsoft.Json.Linq;

public class DisqualifiedMsg : Msg
{
	#region public instance fields
	public VehicleId VehicleId { get; protected set; }
	public string Reason { get; protected set; }
	public string GameId { get; protected set; }
	public int GameTick { get; protected set; }
	#endregion

	#region protected instance properties
	protected override string MessageType{ get{ return MsgType.Disqualified; } }
	#endregion

	#region public instance constructors
	public DisqualifiedMsg(JObject json, string gameId, int gameTick) : this(
		new VehicleId(
			json ["car"]["name"].ToString(), 
			json ["car"]["color"].ToString()
		),
		json ["reason"].ToString(),
		gameId,
		gameTick
	){}

	public DisqualifiedMsg(VehicleId vehicleId, string reason, string gameId, int gameTick)
	{
		this.VehicleId = vehicleId;
		this.Reason = reason;
		this.GameId = gameId;
		this.GameTick = gameTick;
	}
	#endregion
}