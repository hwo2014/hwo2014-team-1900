﻿using System;
using System.Collections.ObjectModel;

public class SimpleVehiclePhysics{
	#region public instance properties
	public double ConstantThrottleCoefficient { get; set; }
	public double ConstantFrictionCoefficient { get; set; }

	public double LinearThrottleCoefficient { get; set; }
	public double LinearFrictionCoefficient { get; set; }

	public double ConstantDriftingCoefficient { get; set; }
	public double LinearDriftingCoefficient { get; set; }

	public double Grip { get; set; }
	#endregion

	#region public instance constructors
	public SimpleVehiclePhysics(
		double throttleCoefficient, 
		double frictionCoefficient,
		double grip
	) : this(
		throttleCoefficient, 
		0.0, 
		0.0, 
		frictionCoefficient, 
		0.0, 
		0.0,
		grip
	){}

	public SimpleVehiclePhysics(
		double constantThrottleCoefficient, 
		double linearThrottleCoefficient, 

		double constantFrictionCoefficient,
		double linearFrictionCoefficient,

		double constantDriftingCoefficient,
		double linearDriftingCoefficient,

		double grip
	){
		this.ConstantThrottleCoefficient = constantThrottleCoefficient;
		this.ConstantFrictionCoefficient = constantFrictionCoefficient;
		this.ConstantDriftingCoefficient = constantDriftingCoefficient;

		this.LinearThrottleCoefficient = linearThrottleCoefficient;
		this.LinearFrictionCoefficient = linearFrictionCoefficient;
		this.LinearDriftingCoefficient = linearDriftingCoefficient;

		this.Grip = grip;
	}
	#endregion

	#region public instance methods
	public virtual double CalculateDrifting(double speed, double radius, double vehicleAngle){
		radius = Math.Min (radius, 100.0);

		return	
			vehicleAngle +
			radius * this.ConstantDriftingCoefficient +
			radius * speed * this.LinearDriftingCoefficient;
	}

	public virtual double CalculateFinalSpeed(double throttle, double currentSpeed, double time){
		throttle = MathHelper.Clamp01(throttle);

		double speed = currentSpeed;
		double absSpeed = Math.Abs(speed);
		double signSpeed = (double)(Math.Sign(speed));

		return	currentSpeed + 
				time *
				(
					throttle * (this.ConstantThrottleCoefficient + absSpeed * this.LinearThrottleCoefficient) -
					signSpeed * (this.ConstantFrictionCoefficient +  absSpeed * this.LinearFrictionCoefficient)
				);
	}

	public virtual double CalculateInitialSpeed(double throttle, double finalSpeed, double time){
		throttle = MathHelper.Clamp01(throttle);
		/*
		0 
		==
		- finalSpeed
		+ time * throttle * this.ConstantThrottleCoefficient
		- time * this.ConstantFrictionCoefficient 
		
		+ initialSpeed
		+ time * throttle * initialSpeed * this.LinearThrottleCoefficient
		- time * initialSpeed * this.LinearFrictionCoefficient)
		*/

		double a =
			+ 1
			- time * this.LinearFrictionCoefficient
			+ time * this.LinearThrottleCoefficient * throttle;

		double b = 
			- finalSpeed
			- time * this.ConstantFrictionCoefficient
			+ time * this.ConstantThrottleCoefficient * throttle;

		return MathHelper.SolveFirstDegreeEquation (a, b);
	}

	public virtual double CalculateOptimalThrottle(
		double currentSpeed, 
		double targetSpeed,
		double time
	){
		double speed = currentSpeed;
		double absSpeed = Math.Abs(speed);
		double signSpeed = (double)(Math.Sign(speed));

		return 
			(
				(targetSpeed - currentSpeed) / time +
				signSpeed * (this.ConstantFrictionCoefficient + absSpeed * this.LinearFrictionCoefficient)
			)
			/
			(
				this.ConstantThrottleCoefficient +
				absSpeed * this.LinearThrottleCoefficient
			);
	}

	public virtual double CalculateSpeedRequiredToReachTargetDrifting(
		double radius, 
		double currentDrifting,
		double targetDrifting
	){
		radius = Math.Min (radius, 100.0);

		return	(targetDrifting - currentDrifting - radius * this.ConstantDriftingCoefficient) /
				(radius * this.LinearDriftingCoefficient);
	}


	public virtual double CalculateSpeedRequiredToMaintainDrifting(double radius, double currentDrifting){
		return this.CalculateSpeedRequiredToReachTargetDrifting (radius, currentDrifting, currentDrifting);

	}

	public virtual double CalculateThrottleRequiredToMaintainSpeed(double currentSpeed){
		return CalculateOptimalThrottle (currentSpeed, currentSpeed, 1.0);
	}

	public virtual double GetAcceleration(double currentSpeed){
		return this.GetAcceleration (currentSpeed, 1.0);
	}

	public virtual double GetAcceleration(double currentSpeed, double throttle){
		throttle = MathHelper.Clamp01(throttle);

		double speed = currentSpeed;
		double absSpeed = Math.Abs(speed);

		return	throttle * (
		    this.ConstantThrottleCoefficient +
		    absSpeed * this.LinearThrottleCoefficient
		);
	}

	public virtual double GetDeceleration(double currentSpeed){
		double speed = currentSpeed;
		double absSpeed = Math.Abs(speed);
		double signSpeed = (double)(Math.Sign(speed));

		return signSpeed * (this.ConstantFrictionCoefficient + absSpeed * this.LinearFrictionCoefficient);
	}
	#endregion
}