﻿using System;
using System.Collections.Generic;

public class AI
{
	#region public class definitions
	public class GameTickInformation
	{
		public double InitialSpeed { get; protected set; }
		public double InitialDrifting { get; protected set; }
		public double TrackRadius { get; protected set; }
		public double Throttle { get; protected set; }
		public double Time { get; protected set; }
		public double FinalSpeed { get; protected set; }
		public double FinalDrifting { get; protected set; }

		public GameTickInformation(
			double initialSpeed,
			double initialDrifting,
			double trackRadius,
			double throttle,
			double time,
			double finalSpeed,
			double finalDrifting
		){
			this.InitialSpeed = initialSpeed;
			this.InitialDrifting = initialDrifting;
			this.TrackRadius = trackRadius;
			this.Throttle = throttle;
			this.Time = time;
			this.FinalSpeed = finalSpeed;
			this.FinalDrifting = finalDrifting;
		}
	}
	#endregion

	#region public event definitions
	public event Action<double> OnThrottle;
	public event Action<SwitchLaneDirection> OnSwitchLane;
	#endregion

	#region public instance properties
	public Track Track { get; protected set; }
	public SimpleVehiclePhysics VehiclePhysics { get; protected set; }
	public Vehicle Vehicle { get; protected set; }

	public Dictionary<VehicleId, Vehicle> Opponents { get; protected set; }
	public List<CrashInformation> Crashes { get; protected set; }
	public List<GameTickInformation> RaceHistory { get; protected set; }

	public double Throttle { get; protected set; }
	public double ExpectedSpeed { get; protected set; }
	public double TargetSpeed { get; protected set; }
	public bool LearningMode { get; protected set; }
	#endregion

	#region protected instance fields
	protected VehicleId _id;
	protected bool _maxSpeedsCalculated = false;
	#endregion

	#region public constructor
	public AI (SimpleVehiclePhysics vehiclePhysics, bool learningMode)
	{
		this.LearningMode = learningMode;
		this.VehiclePhysics = vehiclePhysics;
		this.Opponents = new Dictionary<VehicleId, Vehicle> ();
		this.RaceHistory = new List<GameTickInformation> ();
		this.Crashes = new List<CrashInformation> ();
	}
	#endregion

	#region public instance methods
	public virtual void OnCrash(CrashMsg msg){
		if (msg != null && msg.VehicleId != null) {
			if (msg.VehicleId.Equals (this.Vehicle.Id)) {
				this.OnCrashed (msg.GameTick);
			} else {
				this.OnOpponentCrashed (this.Opponents[msg.VehicleId], msg.GameTick);
			}
		}
	}

	public virtual void OnDisqualified(DisqualifiedMsg msg){
		if (msg != null && msg.VehicleId != null) {
			if (msg.VehicleId.Equals (this.Vehicle.Id)) {
				this.OnDisqualified (msg.Reason, msg.GameTick);
			} else {
				this.OnOpponentDisqualified (this.Opponents[msg.VehicleId], msg.Reason, msg.GameTick);
			}
		}
	}

	public virtual void OnJoined(JoinedMsg msg){
		if (msg != null && msg.Id != null) {
			this._id = msg.Id;
		}
	}

	public virtual void OnGameInitialized(GameInitializedMsg msg){
		if (msg != null && msg.Track != null && msg.Vehicles != null) {
			this.Track = new Track (msg.Track);

			// Iterate over all the vehicles in the message...
			foreach (GameInitializedMsg.VehicleInformation vehicle in msg.Vehicles) {
				// Check if the vehicle is correctly defined...
				if (vehicle != null && vehicle.Id != null && vehicle.Dimensions != null){
					Vehicle v = new Vehicle (vehicle.Id, vehicle.Dimensions, this.Track);

					// In that case, check if it's the AI's vehicle or an opponent's vehicle...
					if (vehicle.Id.Equals (this._id)) {
						this.Vehicle = v;
					} else {
						// If it's an opponent vehicle...
						this.Opponents [v.Id] = v;
					}
				}
			}

			// Calculate the max speed at each sector of the track
			this.CalculateMaxSpeed ();
		}
	}

	public virtual void OnGameStarted(GameStartedMsg msg){
		this.Vehicle.OnRaceStarted ();

		foreach (Vehicle opponent in this.Opponents.Values) {
			opponent.OnRaceStarted ();
		}
	}

	public virtual void OnGameFinished(GameFinishedMsg msg){
		this.Vehicle.OnRaceFinished ();

		foreach (Vehicle opponent in this.Opponents.Values) {
			opponent.OnRaceFinished ();
		}
	}

	public virtual void OnLapFinished(LapFinishedMsg msg){
		if (msg != null && msg.VehicleId != null) {
			if (this.Vehicle.Id.Equals (msg.VehicleId)) {
				this.Vehicle.OnLapFinished ();
			} else {
				this.Opponents [msg.VehicleId].OnLapFinished ();
			}
		}
	}

	public virtual void OnRaceFinished(RaceFinishedMsg msg){
		if (msg != null && msg.VehicleId != null) {
			if (this.Vehicle.Id.Equals (msg.VehicleId)) {
				//this.Vehicle.OnLapFinished ();
			} else {
				//this.Opponents [msg.VehicleId].OnLapFinished ();
			}
		}
	}

	public virtual void OnSpawn(SpawnMsg msg){
		if (msg != null && msg.VehicleId != null) {
			if (msg.VehicleId.Equals (this.Vehicle.Id)) {
				this.OnRespawned (msg.GameTick);
			} else {
				this.OnOpponentRespawned (this.Opponents[msg.VehicleId], msg.GameTick);
			}
		}
	}

	public virtual void OnUpdate(CarPositionsMsg msg){
		if (msg != null && msg.Vehicles != null) {
			// Iterate over all the vehicles in the message...
			foreach (CarPositionsMsg.VehicleInformation vehicle in msg.Vehicles) {
				// Check if the vehicle is correctly defined...
				if (vehicle != null && vehicle.Id != null && vehicle.Position != null){
					// In that case, check if it's the AI's vehicle or an opponent's vehicle
					if (vehicle.Id.Equals (this.Vehicle.Id)) {
						// If it's the AI's vehicle...
						this.Vehicle.Update (vehicle.Position, msg.GameTick);
					} else {
						// If it's an opponent vehicle...
						this.Opponents [vehicle.Id].Update (vehicle.Position, msg.GameTick);
					}
				}
			}

			// After updating the information of every vehicle, let the AI decide its next movement
			this.Drive ();
		}
	}
	#endregion

	#region protected instance methods
	protected virtual void CalculateMaxSpeed(){
		int maxIterations = 30;
		bool changed = true;
		int iterations = 0;

		foreach (RacingLine racingLine in this.Track.RacingLines) {
			// Cache the most used information
			int count = racingLine.Sectors.Count;

			double minSpeed = MathHelper.GetMaxSpeed(
				Math.Min(racingLine.Sectors[racingLine.TighestCorner].Radius, 10000.0),
				this.VehiclePhysics.Grip * racingLine.Sectors[racingLine.TighestCorner].GetGrip()
			);

			// PASS 1: calculate the max speed taking into account the turning radius
			foreach (Sector sector in racingLine.Sectors){
				double speed = MathHelper.GetMaxSpeed(
					Math.Min(sector.Radius, 10000.0),
					this.VehiclePhysics.Grip * sector.GetGrip()
				);
				/*
				for (int i = 0; i < maxIterations; ++i) {
					double currentDrifting = this.VehiclePhysics.CalculateDrifting (
						speed,
						sector.Radius,
						0f
					);

					speed = this.VehiclePhysics.CalculateSpeedRequiredToReachTargetDrifting (
						sector.Radius
						,
						currentDrifting
						,
						59.0
					);
				}
				*/
				sector.SetMaxSpeed (speed);
				sector.SetEstimatedSpeed (speed);
			}

			// PASS 2: calculate the max speed taking into account the acceleration and deceleration modifiers:
			// We continue iterating until there the max speed remain constant in all nodes
			// Or until we have complete the desired number of iterations
			while (iterations < maxIterations && changed){
				changed = false;
				++iterations;

				for (int currentSectorIndex = 0; currentSectorIndex < count; ++currentSectorIndex){
					Sector previousSector = racingLine.Sectors [(currentSectorIndex + count - 1) % count]; 
					Sector currentSector = racingLine.Sectors [currentSectorIndex];
					Sector nextSector = racingLine.Sectors [(currentSectorIndex + count + 1) % count]; 

					// Store the previous speed
					double oldSpeed = currentSector.GetMaxSpeed ();

					// Calculate the max speed at the beginning of the current sector
					double nextSpeed = nextSector.GetMaxSpeed ();
					double distance = nextSector.Length;
					double currentSpeed;

					do {
						currentSpeed = MathHelper.Clamp(
							this.VehiclePhysics.CalculateInitialSpeed(0.0, nextSpeed, 1.0),
							minSpeed,
							oldSpeed
						);
						nextSpeed = currentSpeed;
						distance -= currentSpeed;
					} while (distance > 0.0);

					currentSector.SetMaxSpeed(Math.Min(
						currentSector.GetMaxSpeed (),
						currentSector.GetDeceleration() * currentSpeed
					));

					// Calculate the speed increment since the beginning of the previous sector
					double previousSpeed = previousSector.GetEstimatedSpeed ();
					distance = previousSector.Length;

					do {
						currentSpeed = this.VehiclePhysics.CalculateFinalSpeed(1.0, previousSpeed, 1.0);
						previousSpeed = currentSpeed;
						distance -= currentSpeed;
					} while (distance > 0.0);

					currentSector.SetEstimatedSpeed(Math.Min(
						currentSector.GetMaxSpeed (),
						currentSector.GetAcceleration() * currentSpeed
					));


					currentSector.SetMaxSpeed (Math.Min (
						currentSector.GetMaxSpeed (),
						currentSector.GetEstimatedSpeed() * 2.0
					));

					// Check if the speed has changed
					changed = changed || (Math.Abs(oldSpeed - currentSector.GetMaxSpeed()) < double.Epsilon);
				}
			}
		}

		this._maxSpeedsCalculated = true;
	}

	protected virtual void Drive (){
		if (this.Track != null && this.Vehicle != null && this.Vehicle.CurrentPosition != null) {
			// Store the old throttle value
			double previousThrottle = this.Throttle;

			if (this._maxSpeedsCalculated) {
				// Calculate the current speed and the target speed
				int laneIndex = this.Vehicle.CurrentPosition.StartLane;
				int sectorIndex = this.Vehicle.CurrentPosition.CurrentPieceIndex;
				int nextSectorIndex = (sectorIndex + 1) % this.Track.SectorCount;

				double sectorProgress = this.Vehicle.GetSectorProgress ();
				Sector currentSector = this.Track.GetSector (laneIndex, sectorIndex);
				Sector nextSector = this.Track.GetSector (laneIndex, nextSectorIndex);

				double currentSpeed = this.Vehicle.CurrentSpeed;

				this.TargetSpeed = Math.Abs (
					sectorProgress * nextSector.GetMaxSpeed () +
					(1.0 - sectorProgress) * currentSector.GetMaxSpeed ()
				);

				this.ExpectedSpeed = currentSector.GetEstimatedSpeed ();
				//this.TargetSpeed = currentSector.GetMaxSpeed ();

				if (this.LearningMode) {
					// Set the throttle, brake and steering values under normal driving circunstances
					double maxThrottleSpeed = 0.975f * this.TargetSpeed;
					double brakeSpeed = 1.05f * this.TargetSpeed;

					if (currentSpeed < maxThrottleSpeed) {
						this.Throttle = 1f;
					} else if (currentSpeed > brakeSpeed) {
						this.Throttle = 0f;
					} else {
						this.Throttle = MathHelper.Clamp01 (
							(brakeSpeed - currentSpeed) / (brakeSpeed - maxThrottleSpeed)
						);
					}

					// Check if the vehicle is drifting too much
					if (Math.Abs(this.Vehicle.CurrentPosition.Angle) >= 59.0) {
						if (this.Throttle > 0.1) {
							Console.WriteLine (this.Throttle + " - " + this.Vehicle.CurrentPosition.Angle + "!!!!\n\n\n");
						}

						this.Throttle = 0.0;
					}
				} else {
					this.Throttle = MathHelper.Clamp01 (this.VehiclePhysics.CalculateOptimalThrottle (
						currentSpeed,
						this.TargetSpeed,
						1.0
					));

					// If we aren't braking, check if we can throttle a little more
					double currentDrifting = this.Vehicle.CurrentPosition.Angle;
					double signDrifting = (double) Math.Sign (currentDrifting);
					double absDrifting = Math.Abs (currentDrifting);

					if (this.Throttle > 0.1){
						if (absDrifting < 50.0) {
							/*
							this.TargetSpeed = Math.Max (
								this.TargetSpeed
								,
								this.VehiclePhysics.CalculateSpeedRequiredToReachTargetDrifting (
									currentSector.Radius,
									currentDrifting,
									signDrifting * 50.0
								)
							);
							*/
						} else if (absDrifting > 59.0) {
							this.TargetSpeed = 0.0;
						} else {
							/*
							this.TargetSpeed = Math.Min (
								this.TargetSpeed
								,
								this.VehiclePhysics.CalculateSpeedRequiredToMaintainDrifting (
									currentSector.Radius,
									currentDrifting
								)
							);
							*/
						}

						this.Throttle = MathHelper.Clamp01 (this.VehiclePhysics.CalculateOptimalThrottle (
							currentSpeed,
							this.TargetSpeed,
							1.0
						));
					}
				}



				// Store the race history
				if (this.LearningMode && !this.Vehicle.Crashed) {
					this.RaceHistory.Add (new GameTickInformation (
						this.Vehicle.PreviousSpeed,
						this.Vehicle.PreviousPosition.Angle,
						currentSector.Radius,
						this.Throttle,
						1.0,
						this.Vehicle.CurrentSpeed,
						this.Vehicle.CurrentPosition.Angle
					));
				}
			}else{
				this.Throttle = 1.0;
			}

			// TODO: check if we want to change lanes
			if (this.OnThrottle != null) {
				this.OnThrottle (this.Throttle);
			}
		}
	}

	protected virtual void OnDisqualified(string reason, int gameTick){
		this.Vehicle.Disqualfied = true;
	}

	protected virtual void OnCrashed(int gameTick){
		this.Vehicle.Crashed = true;

		CrashInformation crash = new CrashInformation (
			this.Vehicle.CurrentSpeed,
			this.Track.GetSector (
				this.Vehicle.CurrentPosition.StartLane, 
				this.Vehicle.CurrentPosition.CurrentPieceIndex
			).Angle,
			this.Vehicle.CurrentPosition.Angle,
			gameTick
	     );


		Console.WriteLine (
			"\n\n\n\n\n\nCRASH: " +
			crash.Speed + " | " +
			crash.TrackAngle + " | " +
			crash.DriftingAngle + " | " +
			crash.GameTick
		);

		this.Crashes.Add (crash);
	}

	protected virtual void OnRespawned(int gameTick){
		this.Vehicle.Crashed = false;
	}

	protected virtual void OnOpponentDisqualified(Vehicle vehicle, string reason, int gameTick){
		vehicle.Disqualfied = true;
	}

	protected virtual void OnOpponentCrashed(Vehicle vehicle, int gameTick){
		vehicle.Crashed = true;
	}

	protected virtual void OnOpponentRespawned(Vehicle vehicle, int gameTick){
		vehicle.Crashed = false;
	}
	#endregion
}

