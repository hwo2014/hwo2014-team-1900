﻿using System;

public class VehicleDimensionsDBO
{
	#region public instance properties
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/// <summary>
	/// Gets or sets the guide flag position: the distance between the front part of the vehicle and the guide flag
	/// that acts as "center of gravity" of the vehicle.
	/// </summary>
	/// <value>The guide flag position.</value>
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public double GuideFlagPosition{get; set;}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/// <summary>
	/// Gets or sets the length of the vehicle: the distance between the front and the rear part of the vehicle.
	/// </summary>
	/// <value>The length of the vehicle.</value>
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public double Length {get; set;}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/// <summary>
	/// Gets or sets the width of the vehicle: the distance between the left and the right part of the vehicle.
	/// </summary>
	/// <value>The width of the vehicle.</value>
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public double Width {get; set;}
	#endregion

	#region public instance constructors
	public VehicleDimensionsDBO (double length, double width, double guideFlagPosition)
	{
		this.Length = length;
		this.Width = width;
		this.GuideFlagPosition = guideFlagPosition;
	}
	#endregion
}