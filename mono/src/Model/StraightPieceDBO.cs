public class StraightPieceDBO : TrackPieceDBO
{
	#region public instance properties
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/// <summary>
	/// Gets or sets the length of the track piece.
	/// </summary>
	/// <value>The length.</value>
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public double Length {get; set;}
	#endregion

	#region public instance constructor
	public StraightPieceDBO(double length, bool canSwitchLane, bool isBridge) : base(canSwitchLane, isBridge)
	{
		this.Length = length;
	}
	#endregion
}