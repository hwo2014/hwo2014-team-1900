﻿using System;

public class VehicleId : IEquatable<VehicleId>
{
	#region public instance properties
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/// <summary>
	/// Gets or sets the color that identifies the bot on the track.
	/// </summary>
	/// <value>The color of the vehicle.</value>
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public string Color {get; set;}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/// <summary>
	/// Gets or sets the name. Maximum length for bot name is 16 characters. 
	/// </summary>
	/// <value>The name of the bot.</value>
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public string Name{
		get{
			return this.__name;
		}
		set{
			this.__name = value != null ? value.Substring(0, Math.Min(16, value.Length)) : null;
		}
	}
	#endregion

	#region private instance properties
	private string __name;
	#endregion

	#region public constructors
	public VehicleId (string name, string color){
		this.Name = name;
		this.Color = color;
	}
	#endregion

	#region IEquatable<VehicleId> implementation
	public virtual bool Equals(VehicleId other){
		return	
			other != null &&
			string.Equals (this.Name, other.Name) && 
			string.Equals (this.Color, other.Color);
	}

	public override bool Equals (object other)
	{
		return this.Equals (other as VehicleId);
	}

	public override int GetHashCode ()
	{
		return 
			(this.Name != null ? this.Name.GetHashCode()	: 0) / 10 +
			(this.Color != null ? this.Color.GetHashCode()	: 0) / 10;
	}
	#endregion
}

