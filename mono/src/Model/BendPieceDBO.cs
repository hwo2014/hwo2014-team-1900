public class BendPieceDBO : TrackPieceDBO
{
	#region public instance properties
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/// <summary>
	/// Gets or sets the angle of the circular segment that the piece forms. For left turns, 
	/// this is a negative value. For a 45-degree bend to the left, the angle would be -45. 
	/// For a 30-degree turn to the right, this would be 30.
	/// https://helloworldopen.com/img/rules/bend.jpg
	/// </summary>
	/// <value>The angle.</value>
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public double Angle {get; set;}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/// <summary>
	/// Gets or sets the radius of the circle that would be formed by this kind of pieces. 
	/// More specifically, this is the radius at the center line of the track.
	/// </summary>
	/// <value>The radius from the center of the track.</value>
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public double Radius {get; set;}
	#endregion

	#region public instance constructors
	public BendPieceDBO(double radius, double angle, bool canSwitchLane, bool isBridge) : base(canSwitchLane, isBridge)
	{
		this.Radius = radius;
		this.Angle = angle;
	}
	#endregion
}

