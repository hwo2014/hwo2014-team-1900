using System;

public class VehiclePositionDBO
{
	#region public instance properties
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/// <summary>
	/// This property depicts the car's slip angle. Normally this is zero, but when you go to a bend fast enough, 
	/// the car's tail will start to drift. Naturally, there are limits to how much you can drift without crashing 
	/// out of the track.
	/// </summary>
	/// <value>The angle between the vehicle and the track.</value>
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public double Angle {get; set;}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/// <summary>
	/// Gets or sets the zero based index of track piece the car is on at this moment.
	/// </summary>
	/// <value>The index of the current track piece.</value>
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public int CurrentPieceIndex {get; set;}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/// <summary>
	/// Gets or sets the distance the car's Guide Flag has traveled from the start of the track piece along 
	/// the current lane.
	/// </summary>
	/// <value>The distance from the start of the current piece.</value>
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public double CurrentPieceDistance {get; set;}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/// <summary>
	/// Gets or sets the lane of the vehicle at the end of the current tick.
	/// </summary>
	/// <value>The lane at the end of the current tick.</value>
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public int EndLane {get; set;}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/// <summary>
	/// Gets or sets the number of laps the car has completed. The number 0 indicates that the car is on its 
	/// first lap. The number -1 indicates that it has not yet crossed the start line to begin it's first lap.
	/// </summary>
	/// <value>The number of completed laps.</value>
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public int Lap {get; set;}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/// <summary>
	/// Gets or sets the lane of the vehicle at the beginning of the current tick.
	/// </summary>
	/// <value>The lane at the beginning of the current tick.</value>
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public int StartLane {get; set;}
	#endregion

	#region public constructors
	public VehiclePositionDBO(
		int lap, 
		double angle, 
		int currentPieceIndex, 
		double currentPieceDistance, 
		int startLane,
		int endLane
	){
		this.Angle = angle;
		this.CurrentPieceIndex = currentPieceIndex;
		this.CurrentPieceDistance = currentPieceDistance;
		this.EndLane = endLane;
		this.Lap = lap;
		this.StartLane = startLane;
	}
	
	#endregion
}