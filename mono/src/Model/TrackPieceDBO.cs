using System;

public class TrackPieceDBO
{
	#region public instance properties
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/// <summary>
	/// Gets or sets a value indicating whether the vehicle can switch lanes in this piece.
	/// </summary>
	/// <value><c>true</c> if the vehicle can switch lanes; otherwise, <c>false</c>.</value>
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public bool CanSwitchLanes {get; set;}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/// <summary>
	/// Gets or sets a value indicating whether this instance is bridge.This attribute is merely a visualization 
	/// cue and does not affect the actual game.
	/// </summary>
	/// <value><c>true</c> if this instance is a bridge; otherwise, <c>false</c>.</value>
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public bool IsBridge {get; set;}
	#endregion

	#region public constructors
	public TrackPieceDBO (bool canSwitchLane, bool isBridge)
	{
		this.CanSwitchLanes = canSwitchLane;
		this.IsBridge = isBridge;
	}
	#endregion

	#region public abstract instance methods
	// public abstract double GetLength();
	#endregion
}

