﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

public class TrackDBO
{
	#region public instance properties
	public string Id {get; set;}
	public string Name {get; set;}
	public double StartingPointAngle {get; set;}
	public double StartingPointX {get; set;}
	public double StartingPointY {get; set;}

	public ReadOnlyCollection<TrackPieceDBO> Pieces { get; protected set; }
	public ReadOnlyCollection<LaneDBO> Lanes { get; protected set; }
	#endregion

	#region public instance constructors
	public TrackDBO (
		string id,
		string name,
		double startingPointAngle,
		double startingPointX,
		double startingPointY,
		IEnumerable<TrackPieceDBO> pieces,
		IEnumerable<LaneDBO> lanes
	){
		List<TrackPieceDBO> p = new List<TrackPieceDBO>();
		if (pieces != null) {
			foreach (TrackPieceDBO piece in pieces) {
				if (piece != null) {
					p.Add (piece);
				}
			}
		}

		List<LaneDBO> l = new List<LaneDBO>();
		if (lanes != null) {
			foreach (LaneDBO lane in lanes) {
				if (lane != null) {
					l.Add (lane);
				}
			}
		}

		this.Id = id;
		this.Name = name;
		this.StartingPointAngle = startingPointAngle;
		this.StartingPointX = startingPointX;
		this.StartingPointY = startingPointY;
		this.Pieces = new ReadOnlyCollection<TrackPieceDBO> (p);
		this.Lanes = new ReadOnlyCollection<LaneDBO> (l);
	}
	#endregion
}
